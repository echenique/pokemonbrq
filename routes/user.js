const Router        = require('express').Router;
const httpUtils     = require('./../util/http');
const UserModel     = require('./../model/UserModel');
const crypto        = require('crypto-js');
const config        = require('./../config/index');
const uuid          = require('uuidv4')
const jwt           = require('jsonwebtoken');

let route = new Router();

/**
 * @swagger
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required: 
 *              - nickname
 *              - password
 *              - mail
 *          properties:
 *              nickname:
 *                  type: String
 *                  description: nickname utilizado para efetuar o login
 *              password:
 *                  type: String
 *                  description: Senha do usuário
 *              mail:
 *                  type: String
 *                  description: E-mail do usuário
 *      Login:
 *          type: object
 *          required:
 *              - nickname
 *              - password
 *          properties:
 *              nickname:
 *                  type: String
 *                  description: nickname utilizado para efetuar o login
 *              password:
 *                  type: String
 *                  description: Senha do usuário
 */


/**
 * @swagger
 * /user/login:
 *  post: 
 *      sumary: Efetua um Login
 *      tags: [Users]
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json: 
 *                  schema: 
 *                      $ref: '#/components/schemas/Login'
 *      responses:
 *          200:
 *              description: Usuário logado corretamente.
 *          400:
 *              description: Usuário ou senha inválidos.
 *          500:
 *              description: Internal Error.
 */
route.post('/login', async (req, res) => {
    let credentials = {
		nickname: req.body.nickname,
		password: crypto.MD5(req.body.password, config.secret).toString()
	};
    try{
        console.log('User', credentials);
        let user = await UserModel.findOne(credentials)
        if (!user) {
			return res.status(400).json({message: 'Invalid username or password'});
		}

        let session = {
			id: uuid.uuid(),
			origin: req.get('user-agent'),
			ip: req.ip,
			creationDate: new Date(),
			lastUpdate: new Date(),
			token: jwt.sign({id: user._id}, config.secret, {expiresIn: '7d'}),
		};
		
        if(!UserModel.cache[user._id.toString()]){
            UserModel.cache[user._id.toString()] = {}
        }
        UserModel.cache[user._id.toString()].session = session;
        
        return res.json({
            user: user,
            token: session.token,
        })
    }catch(e){
        console.log(e)
        res.status(500).json(e);
    }
});

/**
 * @swagger
 * /user/new:
 *  post: 
 *      sumary: Registra um novo User
 *      tags: [Users]
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json: 
 *                  schema: 
 *                      $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: Usuário criado com sucesso.
 *          400:
 *              description: Dados Inválidos.
 */
route.post('/new', async (req, res) => {
    if(!req.body.nickname){ res.status(400).json({message: 'Informe o campo \'nickname\''}) }
    if(!req.body.password){ res.status(400).json({message: 'Informe o campo \'password\''}) }
    if(!req.body.mail){ res.status(400).json({message: 'Informe o campo \'mail\''}) }

    let count = await UserModel.count({'nickname': req.body.nickname});
    if(count > 0){ 
        res.status(400).json({message: 'Usuário já cadastrado!'}) 
    }else if(!req.body.password.length){
        res.status(400).json({message: 'Informe uma senha!'});
    }else{
        let newUser = new UserModel({
            nickname: req.body.nickname,
            password: crypto.MD5(req.body.password, config.secret).toString(),
            mail: req.body.mail,
        });
    
        await newUser.save();
        res.json({message: 'Usuário criado!'});
    }
});

/**
 * @swagger
 * /user/{nickname}:
 *  put: 
 *      sumary: Edita um Usuário
 *      tags: [Users]
 *      parameters:
 *          - in: path
 *            name: nickname
 *            schema:
 *              type: string
 *              required: true
 *              description: Nickname do usuário
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json: 
 *                  schema: 
 *                      $ref: '#/components/schemas/User'
 *      responses:
 *          200:
 *              description: Usuário editado.
 *          400:
 *              description: Dados Inválidos.
 *          401: 
 *              description: Efetue um login para utilizar este endpoint
 */
route.put('/:nickname', httpUtils.filter.isLoggedIn, async (req, res) => {
    if(!req.body.nickname){ res.status(400).json({message: 'Informe o campo \'nickname\''}) }
    if(!req.body.password){ res.status(400).json({message: 'Informe o campo \'password\''}) }
    if(!req.body.mail){ res.status(400).json({message: 'Informe o campo \'mail\''}) }

    let editableUser = await UserModel.findOne({nickname: req.params.nickname});
    if(!editableUser){ res.status(400).json({message: `Usuario [${req.params.nickname}] não encontrado.`}) }

    editableUser.nickname = req.body.nickname;
    editableUser.password = crypto.MD5(req.body.password, config.secret).toString();
    editableUser.mail = req.body.mail;
    await editableUser.save();

    res.json({message: 'Usuário editado.'});
});

module.exports = route;