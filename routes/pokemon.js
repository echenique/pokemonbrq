const Router        = require('express').Router;
const httpUtils     = require('./../util/http');
const PokeModel     = require('./../model/PokemonModel');

let route = new Router();

/**
 * @swagger
 * components:
 *  schemas:
 *      Pokemon:
 *          type: object
 *          required: 
 *              - PokemonId
 *              - type
 *              - name
 *          properties:
 *              pokemonId:
 *                  type: String
 *                  description: Numero de registro da Pokedex
 *              type:
 *                  type: String
 *                  description: Tipo do pokemon
 *              name:
 *                  type: String
 *                  description: Nome do pokemon
 */

/**
 * @swagger
 * /pokemon:
 *  get: 
 *      sumary: Lista todos os pokemons
 *      tags: [Pokemons]
 *      responses:
 *          200:
 *              description: Lista completa da pokedex
 *          401: 
 *              description: Efetue um login para utilizar este endpoint
 */
route.get('/', async (req, res) => {
    let list = await PokeModel.find();
    res.json(list)
});

/**
 * @swagger
 * /pokemon/{pokemonId}:
 *  get: 
 *      sumary: Pega as infos de um Pokemon especifico
 *      tags: [Pokemons]
 *      parameters:
 *          - in: path
 *            name: pokemonId
 *            schema:
 *                  type: String
 *                  required: true
 *                  description: Pokemon ID
 *      responses:
 *          200:
 *              description: Detalhes de um Pokemon.
 *          400:
 *              description: Falha ao procurar pokemon.
 *          401: 
 *              description: Efetue um login para utilizar este endpoint
 */

route.get('/:pokemonId', async (req, res) => {
    try{
        let poke = await PokeModel.findOne({pokemonId: req.params.pokemonId.toString()});

        res.json(poke);
    }catch(e){
        res.status(400).json(e);
    }
});

/**
 * @swagger
 * /pokemon/register:
 *  post: 
 *      sumary: Registra um novo Pokemon
 *      tags: [Pokemons]
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json: 
 *                  schema: 
 *                      $ref: '#/components/schemas/Pokemon'
 *      responses:
 *          200:
 *              description: Pokemon registrado.
 *          400:
 *              description: Dados inválidos.
 *          401: 
 *              description: Efetue um login para utilizar este endpoint
 */
route.post('/register', async (req, res) => {
    if(!req.body.name){ res.status(400).json({message: 'Informe o campo \'name\''}) }
    if(!req.body.type){ res.status(400).json({message: 'Informe o campo \'type\''}) }
    if(!req.body.pokemonId){ res.status(400).json({message: 'Informe o campo \'pokemonId\''}) }

    let count = await PokeModel.count({'pokemonId': req.body.pokemonId});
    if(count > 0){ 
        res.status(400).json({message: 'Pokemon já cadastrado!'}) 
    }else{
        let newPokemon = new PokeModel({
            pokemonId: req.body.pokemonId,
            name: req.body.name,
            type: req.body.type,
        });
    
        await newPokemon.save();
        res.json({message: 'Pokemon criado!'});
    }
});

/**
 * @swagger
 * /pokemon/{pokemonId}:
 *  put: 
 *      sumary: Edita um Pokemon
 *      tags: [Pokemons]
 *      parameters:
 *          - in: path
 *            name: pokemonId
 *            schema:
 *              type: string
 *              required: true
 *              description: Pokemon ID
 *      requestBody:
 *          required: true
 *          content: 
 *              application/json: 
 *                  schema: 
 *                      $ref: '#/components/schemas/Pokemon'
 *      responses:
 *          200:
 *              description: Pokemon editado.
 *          400:
 *              description: Dados inválidos.
 *          401: 
 *              description: Efetue um login para utilizar este endpoint
 */
route.put('/:pokemonId', async (req, res) => {
    if(!req.body.name){ res.status(400).json({message: 'Informe o campo \'name\''}) }
    if(!req.body.type){ res.status(400).json({message: 'Informe o campo \'type\''}) }
    if(!req.body.pokemonId){ res.status(400).json({message: 'Informe o campo \'pokemonId\''}) }

    let editablePoke = await PokeModel.findOne({pokemonId: req.params.pokemonId});
    if(!editablePoke){ res.status(400).json({message: `Pokemon ID [${req.params.pokemonId}] não encontrado.`}) }

    editablePoke.name = req.body.name;
    editablePoke.type = req.body.type;
    editablePoke.pokemonId = req.body.pokemonId;
    await editablePoke.save();

    res.json({message: 'Pokemon editado.'});
});

/**
 * @swagger
 * /pokemon/{pokemonId}:
 *  delete: 
 *      sumary: Apaga as infos de um Pokemon especifico
 *      tags: [Pokemons]
 *      parameters:
 *          - in: path
 *            name: pokemonId
 *            schema:
 *                  type: String
 *                  required: true
 *                  description: Pokemon ID
 *      responses:
 *          200:
 *              description: Pokemon deletado.
 *          400:
 *              description: Erro ao deletar pokemon.
 *          401: 
 *              description: Efetue um login para utilizar este endpoint
 */
route.delete('/:pokemonId', async (req, res) => {
    try{
        await PokeModel.deleteOne({pokemonId: req.params.pokemonId});
        res.json({message: 'Pokemon Deleted!'})
    }catch(e){
        res.status(400).json(e);
    }
});

module.exports = route;