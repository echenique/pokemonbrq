module.exports = {
    db: {
        host: 'localhost',
        port: '27017',
        name: 'pokemon'
    },
    http: {
        host: 'localhost',
        port: 3000
    },
    secret: "P0k3m0N",
}