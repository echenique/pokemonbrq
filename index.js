const mongoose      = require('mongoose');
const express       = require('express');
const BP 			= require('body-parser');
const config        = require('./config/index');
const httpUtils     = require('./util/http');
const userRoute		= require('./routes/user');
const pokeRoute		= require('./routes/pokemon');
const swaggerUI		= require('swagger-ui-express');
const swaggerJSDoc	= require('swagger-jsdoc');

const setup = {
	httpServer: {},
    dataBase: (dbType) => {
        switch (dbType) {
			case 'mongo':
			default:
				return new Promise((resolve, reject) => {
					console.log('BOOTSTRAP','DATABASE','Preparing to connect on : PokemonDB');
					mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.name}`);
					mongoose.connection.on('error', reject);
					mongoose.connection.once('open', () => {
						console.log('BOOTSTRAP','DATABASE','Connected!');
						resolve();
					});
				});
			break;

			case 'postgre':
				//Implementar conexão com Postgre.
			break;
		}
		
    },
    apiServer: () => {
        return new Promise((resolve, reject) => {
			console.log('BOOTSTRAP','HTTP-SERVER','Preparing HTTP Server with args:', config.http);

			this.httpServer = express();

			this.httpServer
            .use(BP.urlencoded({extended: true}))
            .use(BP.json())
            .use(httpUtils.filter.errorHandler)
            .use(httpUtils.filter.noCache);


			this.httpServer.listen(config.http.port, () => {
				console.log('BOOTSTRAP','HTTP-SERVER','Ready at:', `http://${config.http.host}:${config.http.port}/`);
				resolve();
			}).on('error', (err) => {
				reject(err);
			});

			this.httpServer.timeout = 120000 * 10;
		});
    },
	swaggerServer: () => {
		return new Promise((resolve, reject) => {
			const opt = {
				definition: {
					openapi: '3.0.0',
					info: {
						title: 'Library API',
						version: '1.0.0',
						description: 'Simple Library API'
					},
					components: {
						securitySchemes: {
						  	bearerAuth: {
								type: 'http',
								scheme: 'bearer',
								bearerFormat: 'JWT',
						  	}
						}
					}
				},
				apis: ['./routes/*.js']
			}
			const specs = swaggerJSDoc(opt);

			this.httpServer.use('/api-docs', swaggerUI.serve, swaggerUI.setup(specs))

			resolve();
		})
	},
	routes: () => {
		return new Promise((resolve, reject) => {
			this.httpServer.use('/user', userRoute);
			this.httpServer.use('/pokemon', httpUtils.filter.isLoggedIn);
			this.httpServer.use('/pokemon', pokeRoute);

			resolve();
		})
	}
}

setup.dataBase()
.then(setup.apiServer())
.then(setup.swaggerServer())
.then(setup.routes())
.catch(e => {
    console.log(e);
    process.exit();
});