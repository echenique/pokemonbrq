## Pré reqs
 - MongoDB (Docker)
 - NodeJS
 - Npm


## Setup Mongo via docker
``docker run -d -p 27017-27019:27017-27019 --name mongodb mongo:4.0.4``

### Comandos:
`npm run setup`
Faz o restore do banco, instala as libs e roda o server.

``npm run mongorestore``
Sobe o backup do banco

``npm run server``
Roda o servidor

### Postman
Collection: 'PokeBRQ.postman_collection.json'

### API Docs
Documentação disponivel em: `/api-docs`