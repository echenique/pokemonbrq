let mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	nickname: String,
	password: String,
	mail: String
}, {usePushEach: true});

const model = mongoose.model('User', userSchema);

model.cache = [];

module.exports = model;
