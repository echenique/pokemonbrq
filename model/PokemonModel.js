let mongoose = require('mongoose');

const pokemonSchema = new mongoose.Schema({
	pokemonId: String,
	name: String,
	type: String
}, {usePushEach: true});

const model = mongoose.model('Pokemon', pokemonSchema);

module.exports = model;
