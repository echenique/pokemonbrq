const jwt 			= require('jsonwebtoken');
const UserModel 	= require('../model/UserModel');
const config 		= require('./../config/index');

module.exports = {
	filter: {
		errorHandler: (req, res, next) => {
			res.error = (err, status) => {

				if (err && err.stack){
					console.log(err);
					console.log(err.stack);

					res.status(status || 500).json({
						message: err.message || err || 'Internal Server Error',
					});
				} else {
					console.log(err);

					res.status(status || 500).json({
						message: 'Internal Server Error',
					});
				}
			};
			next();
		},

		noCache: (req, res, next) => {
			res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
			res.header('Expires', '-1');
			res.header('Pragma', 'no-cache');
			next();
		},

		isLoggedIn: (req, res, next) => {

			let token = req.get('Auth-Token');

			if (!token){
				return res.error(new Error('1 - Usuário não logado.'), 401);
			}

			jwt.verify(token, config.secret, (err, decoded) => {
				if (err) {
					if (err.name === 'TokenExpiredError'){
						return res.error(new Error('Expired session'), 401);
					} else {
						return res.error(new Error('2 - Usuário não logado.'), 401);
					}
				}

				let user = UserModel.cache[decoded.id];
				if (err || !user){
					return res.error(new Error('3 - Usuário não logado.'), 401);
				}

				if (!user.session){
					return res.error(new Error('4 - Usuário não logado.'), 401);
				}

				req.user = user;
				req.authSession = user.session;
				next();
			});
		}
	}
};
